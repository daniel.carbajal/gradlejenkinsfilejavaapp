package greeter

import groovy.transform.CompileStatic
import com.github.lalyos.jfiglet.FigletFont

@CompileStatic
class GreetingFormatter {
    static String greeting(final String name) {
        FigletFont.convertOneLine("Hello, ${name.capitalize()}")
    }
}

